package se.josoder.exam.frontend.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.html.HTMLDListElement;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 * Created by josoder on 07.06.17.
 */
public class MenuPageObject extends PageObject {
    public MenuPageObject(WebDriver driver) {
        super(driver);
    }


    public boolean isOnPage() {
        return driver.getTitle().equals("MyCantina Menus");
    }

    public boolean isSelectable(String... dishes){
        for(String d:dishes) {
            List<WebElement> elements = driver.findElements(
                    By.xpath("//table[@id='createMenu:menuDishTable']//tbody/tr/td/span[contains(text(), '"+d+"')]"));
            if(elements.isEmpty()){
                return false;
            }
        }
        return true;
    }

    public HomePageObject createMenuWithSelected(String date, String... dishes){
        setText("createMenu:date", date);

        for (String d : dishes){
             WebElement e = driver.findElement(By.xpath("//table[@id='createMenu:menuDishTable']//tbody/tr[contains(td[1]/span, '"+
                d+"')]/td[2]/input[@type='checkbox']"));
        e.click();
        }

        WebElement createBtn = driver.findElement(By.id("createMenu:createBtn"));
        createBtn.click();
        waitForPageToLoad();

        if(isOnPage()){
            return null;
        } else {
            return new HomePageObject(driver);
        }
    }
}
