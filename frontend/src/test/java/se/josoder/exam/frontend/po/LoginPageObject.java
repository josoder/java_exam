package se.josoder.exam.frontend.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by josoder on 09.06.17.
 */
public class LoginPageObject extends PageObject {
    public LoginPageObject(WebDriver driver) {
        super(driver);
    }

    public boolean isOnPage() {
        return driver.getTitle().equals("Login page");
    }

    public boolean loginUser(String username, String password){
        setText("loginForm:username", username);
        setText("loginForm:password", password);

        WebElement loginBtn = driver.findElement(By.id("loginForm:signInBtn"));
        loginBtn.click();
        waitForPageToLoad();
        return !isOnPage();
    }
}
