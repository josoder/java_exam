package se.josoder.exam.frontend.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by josoder on 07.06.17.
 */
public class DishesPageObject extends PageObject {
    public DishesPageObject(WebDriver driver) {
        super(driver);
    }


    public boolean isOnPage() {
        return driver.getTitle().equals("MyCantina Dishes");
    }

    public void createDish(String name, String description){
        setText("createDishForm:dishName", name);
        setText("createDishForm:dishDescription", description);

        WebElement createBtn = driver.findElement(By.id("createDishForm:createBtn"));
        createBtn.click();
        waitForPageToLoad();
    }

    public boolean containsDish(String name){
        List<WebElement> elements = driver.findElements(
                By.xpath("//table[@id='dishTable']//tbody//tr/td[text()='"+ name +"']"));
        return !elements.isEmpty();
    }
}
