package se.josoder.exam.frontend;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import se.josoder.exam.frontend.po.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by josoder on 10.05.17.
 */
public class WebTestBase {
    private static final AtomicLong counter = new AtomicLong(System.currentTimeMillis());

    protected static HomePageObject home;
    private static WebDriver driver;
    protected static String dish1 = "Capricciosa";
    protected static String dish2 = "Vezuvio";
    protected static String dish3 = "musslor";
    protected static String username = "selenium";
    protected static String password = "testpassword";


    protected WebDriver getDriver(){
        return driver;
    }

    protected String getPageSource(){
        return driver.getPageSource();
    }


    private static boolean tryToSetGeckoIfExists(String property, Path path) {
        if (Files.exists(path)) {
            System.setProperty(property, path.toAbsolutePath().toString());
            return true;
        }
        return false;
    }

    private static void setupDriverExecutable(String executableName, String property) {
        String homeDir = System.getProperty("user.home");

        //first try Linux/Mac executable
        if (!tryToSetGeckoIfExists(property, Paths.get(homeDir, executableName))) {
            //then check if on Windows
            if (!tryToSetGeckoIfExists(property, Paths.get(homeDir, executableName + ".exe"))) {
                fail("Cannot locate the " + executableName + " in your home directory " + homeDir);
            }
        }
    }

    private static WebDriver getChromeDriver() {

        /*
            Need to have Chrome (eg version 53.x) and the Chrome Driver (eg 2.24),
            whose executable should be saved directly under your home directory

            see https://sites.google.com/a/chromium.org/chromedriver/getting-started
         */

        setupDriverExecutable("chromedriver", "webdriver.chrome.driver");

        return new ChromeDriver();
    }

    @BeforeClass
    public static void init() throws InterruptedException {
        driver = getChromeDriver();

        /*
            When the integration tests in this class are run, it might be
            that WildFly is not ready yet, although it was started. So
            we need to wait till it is ready.
         */
        for (int i = 0; i < 30; i++) {
            boolean ready = JBossUtil.isJBossUpAndRunning();
            if (!ready) {
                Thread.sleep(1_000); //check every second
                continue;
            } else {
                break;
            }
        }

    }

    protected static boolean verifyMenuSelected(LocalDate date, String... dishes){
       return home.isMenuOfTheDate(date.toString())&&home.isSelectable(dishes);
    }


    protected boolean createMenu(LocalDate date, String... dishes){
        home.toStartingPage();
        MenuPageObject menu = home.toMenus();
        return menu.createMenuWithSelected(date.toString(), dishes) instanceof HomePageObject;
    }

    protected static void createDishes(String... names){
        for(String d : names){
            createDish(d);
        }
    }

    protected static void createDish(String name){
        home.toStartingPage();
        DishesPageObject dishes = home.toDishes();
        dishes.createDish(name, "food description");
        assertTrue(dishes.containsDish(name));
    }

    // create the menu of today if it does not already exist
    protected static void createMenuForToday(){
        home.toStartingPage();
        if(!home.isMenuOfTheDate(LocalDate.now().toString())){
            createDishes(dish1,dish2,dish3);
            home.toStartingPage();
            MenuPageObject m = home.toMenus();
            // make sure the three dishes are selectable in menus
            assertTrue(m.isSelectable(dish1,dish2,dish3));
            // create the menu with two of the dishes
            m.createMenuWithSelected(LocalDate.now().toString(), dish1,dish2);
        }
    }


    protected static void loginUser(String username, String password){
        LoginPageObject login = home.toLogin();
        assertTrue(login.isOnPage());
        login.loginUser(username, password);
    }

    protected static void createAndLoginUser(){
        if(home.isSignedIn()){
            home.logOut();
        }
        assertTrue(!home.isSignedIn());
        RegisterPageObject r = home.toRegister();
        assertTrue(r.isOnPage());
        r.registerUser("josoder", "password");
        LoginPageObject login = home.toLogin();
        assertTrue(login.isOnPage());
        login.loginUser("josoder", "password");
        assertTrue(home.isSignedIn());
    }


    protected static String getUniqueId() {
        return "foo" + counter.incrementAndGet();
    }

    protected static String getUniqueTitle() {
        return "A title: " + counter.incrementAndGet();
    }


    @AfterClass
    public static void tearDown() {
        driver.close();
    }
}

