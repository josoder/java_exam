package se.josoder.exam.frontend.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by josoder on 09.06.17.
 */
public class RegisterPageObject extends PageObject {
    public RegisterPageObject(WebDriver driver) {
        super(driver);
    }

    public boolean isOnPage() {
        return driver.getTitle().equals("Register User");
    }

    public HomePageObject registerUser(String username, String password){
        setText("registerForm:userName", username);
        setText("registerForm:firstName", "test");
        setText("registerForm:lastName", "tester");
        setText("registerForm:password", password);
        setText("registerForm:confirmPassword", password);

        WebElement loginBtn = driver.findElement(By.id("registerForm:regUserBtn"));
        loginBtn.click();
        waitForPageToLoad();
        return new HomePageObject(driver);
    }
}
