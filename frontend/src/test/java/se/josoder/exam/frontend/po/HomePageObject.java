package se.josoder.exam.frontend.po;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by josoder on 10.05.17.
 */
public class HomePageObject extends PageObject {
    public HomePageObject(WebDriver driver) {
        super(driver);
    }

    public HomePageObject toStartingPage() {
        String context = "/my_cantina";
        driver.get("localhost:8080" + context + "/home.jsf");
        waitForPageToLoad();

        return this;
    }

    public boolean isOnPage() {
        return driver.getTitle().equals("MyCantina Home Page");
    }

    public DishesPageObject toDishes() {
        driver.findElement(By.id("dishesLink")).click();
        waitForPageToLoad();
        return new DishesPageObject(driver);
    }

    public LoginPageObject toLogin(){
        driver.findElement(By.id("loginLink")).click();
        waitForPageToLoad();
        return new LoginPageObject(driver);
    }

    public RegisterPageObject toRegister(){
        driver.findElement(By.id("createUserLink")).click();
        waitForPageToLoad();
        return new RegisterPageObject(driver);
    }

    public MenuPageObject toMenus(){
        driver.findElement(By.id("menusLink")).click();
        waitForPageToLoad();
        return new MenuPageObject(driver);
    }

    public boolean isMenuOfTheDate(String date){
        List<WebElement> h2Tag = driver.findElements(By.tagName("h2"));
        if(h2Tag.isEmpty()) return false;
        String text = h2Tag.get(0).getText();
        return text.equals("Menu for " + date);
    }

    public void clickOnNext(){
        driver.findElement(By.id("changeDateForm:showNext")).click();
        waitForPageToLoad();
    }

    public void clickOnDefault(){
        driver.findElement(By.id("changeDateForm:showDefault")).click();
        waitForPageToLoad();
    }

    public void clickOnPrevious(){
        driver.findElement(By.id("changeDateForm:showPrevious")).click();
        waitForPageToLoad();
    }

    public boolean chefLinksVisible(){
        List<WebElement> dishesLink = driver.findElements(By.id("dishesLink"));
        List<WebElement> menusLink = driver.findElements(By.id("menusLink"));

        return !(menusLink.isEmpty()||dishesLink.isEmpty());
    }


    public boolean isSelectable(String... dishes){
        for(String d:dishes) {
           List <WebElement> e = driver.findElements(By.xpath("//table[@id='changeDateForm:menuTable']//tbody/tr/td/span[text()='"+d+"']"));
           if(e.isEmpty()){
               return false;
           }
        }
        return true;
    }
}
