package se.josoder.exam.frontend;
import org.junit.Before;
import org.junit.Test;
import se.josoder.exam.frontend.po.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;
import static org.junit.Assume.assumeTrue;

/**
 * Created by josoder on 10.05.17.
 */
public class WebPageIT extends WebTestBase {
    @Before
    public void startFromInitialPage() {
        assumeTrue(JBossUtil.isJBossUpAndRunning());

        home = new HomePageObject(getDriver());
        home.toStartingPage();
        createAndLoginUser();
    }

    @Test
    public void testHomePage(){
        assertTrue(home.isOnPage());
    }

    @Test
    public void testCreateDish(){
        DishesPageObject dishes = home.toDishes();
        assertTrue(dishes.isOnPage());
        String newDish = "Taco";
        assertFalse(dishes.containsDish(newDish));
        dishes.createDish(newDish, "mexican food");
        assertTrue(dishes.containsDish(newDish));
    }

    @Test
    public void testMenu(){
        // create 3 dishes
        createMenuForToday();
        // verify that the right menu pops up and that only the dishes chosen is listed (see createMenuForToday())
        home.clickOnDefault();
        assertTrue(home.isMenuOfTheDate(LocalDate.now().toString()));
        assertTrue(home.isSelectable(dish1, dish2));
        assertFalse(home.isSelectable(dish3));
    }

    @Test
    public void testDifferentDates(){
        assertTrue(home.isSignedIn());
        String dishTomorrow = "some food";
        String dishYesterday = "kebab";
        createDishes(dishTomorrow, dishYesterday);
        home.toStartingPage();
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        LocalDate yesterday = LocalDate.now().minusDays(1);
        // create three different menus
        createMenuForToday(); // (creates today's menu if it is not created)
        createMenu(tomorrow, dishTomorrow);
        createMenu(yesterday, dishYesterday);
        // verify that default takes us to today's menu
        home.clickOnDefault();
        assertTrue(verifyMenuSelected(today, dish1, dish2));
        home.clickOnNext();
        assertTrue(verifyMenuSelected(tomorrow, dishTomorrow));
        home.clickOnPrevious();
        assertTrue(verifyMenuSelected(today, dish1, dish2));
        home.clickOnPrevious();
        assertTrue(verifyMenuSelected(yesterday, dishYesterday));
    }

    @Test
    public void testCantSeeChefLinksIfNotLoggedIn(){
        assertTrue(home.isSignedIn());
        assertTrue(home.chefLinksVisible());
        home.logOut();
        assertFalse(home.chefLinksVisible());
    }

    @Test
    public void testLoginWithWrongPassword(){
        home.logOut();
        loginUser(username, "wrong");
        home.toStartingPage();
        assertTrue(!home.isSignedIn());
    }

    @Test
    public void createDishWithoutName(){
        DishesPageObject d = home.toDishes();
        d.createDish("", "wont work..");
        assertFalse(d.containsDish(""));
    }
}
