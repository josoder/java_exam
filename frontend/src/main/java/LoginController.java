import se.josoder.exam.backend.ejb.UserEjb;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by josoder on 08.06.17.
 */
@Named
@SessionScoped
public class LoginController implements Serializable {
    @EJB
    private UserEjb userEjb;

    private String formUsername;
    private String formPassword;
    private String formConfirmPassword;
    private String formFirstName;
    private String formLastname;

    private String loggedOnUser;

    public String createUser(){
        if(!formConfirmPassword.equals(formPassword)){
            return "createUser.jsf";
        }

        boolean valid=false;

        try {
            valid = userEjb.createUser(formUsername, formFirstName, formLastname, formPassword);
        } catch (EJBException e){
            // e.printStackTrace();
        }
        if(!valid){
            return "createUser.jsf";
        }
        return "home.jsf";
    }

    public boolean isLoggedIn(){
        return loggedOnUser!=null;
    }

    public String login(){
        if (userEjb.userLogin(formUsername, formPassword)){
            loggedOnUser = formUsername;
            return "home.jsf";
        } else
        return "login.jsf";
    }

    public String logOut(){
        loggedOnUser = null;
        return "home.jsf";
    }

    public String getFormUsername() {
        return formUsername;
    }

    public void setFormUsername(String formUsername) {
        this.formUsername = formUsername;
    }

    public String getFormPassword() {
        return formPassword;
    }

    public void setFormPassword(String formPassword) {
        this.formPassword = formPassword;
    }

    public String getFormConfirmPassword() {
        return formConfirmPassword;
    }

    public void setFormConfirmPassword(String formConfirmPassword) {
        this.formConfirmPassword = formConfirmPassword;
    }

    public String getFormFirstName() {
        return formFirstName;
    }

    public void setFormFirstName(String formFirstName) {
        this.formFirstName = formFirstName;
    }

    public String getFormLastname() {
        return formLastname;
    }

    public void setFormLastname(String formLastname) {
        this.formLastname = formLastname;
    }

    public String getLoggedOnUser() {
        return loggedOnUser;
    }

    public void setLoggedOnUser(String loggedOnUser) {
        this.loggedOnUser = loggedOnUser;
    }
}
