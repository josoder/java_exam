import se.josoder.exam.backend.ejb.DishEjb;
import se.josoder.exam.backend.ejb.MenuEjb;
import se.josoder.exam.backend.entity.Dish;
import se.josoder.exam.backend.entity.Menu;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by josoder on 07.06.17.
 */
@Named
@SessionScoped
public class MenuController implements Serializable {
    public static final int DEFAULT = 0;
    public static final int NEXT = 1;
    public static final int PREVIOUS = -1;
    @EJB
    private MenuEjb menuEjb;
    @Inject
    private DishEjb dishEjb;
    @Inject
    private LoginController loginController;

    private Map<String, Boolean> menuDishMap;
    private String formDate;
    private int menuChoice = 0;
    private Menu currentMenu = new Menu();


    public String addMenu(){
        if(!loginController.isLoggedIn()){
            return "home.jsf";
        }
        HashSet<Dish> dishes = getDishes();
        System.out.println(dishes.size());
        if(formDate==null||formDate.isEmpty()){
            return "menus.jsf";
        }
        if(dishes.size()==0){
            return "menus.jsf";
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(formDate, dtf);

        Long m = null;
        try {
            m = menuEjb.createMenu(date, dishes);
        } catch (Exception e){
            e.printStackTrace();
        }

        if(m==null){
            return "menus.jsf";
        } else {
            return "home.jsf";
        }
    }

    public List<Dish> getAllDishes(){
        List<Dish> dishes = dishEjb.getAllDishes();
        menuDishMap = new HashMap<>();

        dishes.stream().forEach(d ->
            menuDishMap.put(d.getName(), false));

        return dishes;
    }

    private HashSet<Dish> getDishes(){
        return menuDishMap.entrySet().stream()
                .filter(m -> m.getValue()==true)
                .map(m -> dishEjb.getDish(m.getKey()))
                .collect(Collectors.toCollection(HashSet::new));
    }

    public void onClick(String name, boolean value){
        menuDishMap.put(name, value);
    }

    public String getFormDate() {
        return formDate;
    }

    public void setFormDate(String formDate) {
        this.formDate = formDate;
    }

    public Map<String, Boolean> getMenuDishMap() {
        return menuDishMap;
    }

    public void setMenuDishMap(Map<String, Boolean> menuDishMap) {
        this.menuDishMap = menuDishMap;
    }


    // on click
    public String editMenuChoice(int id){
        if(!loginController.isLoggedIn()){
            return "home.jsf";
        }
        this.menuChoice = id;
        setCurrent();
        return "home.jsf";
    }

    private void setCurrent(){
        if(menuChoice==PREVIOUS && hasPrevious()){
            currentMenu = getPrevious();
        } else if(menuChoice==DEFAULT){
            currentMenu = menuEjb.getMenuForDate(LocalDate.now());
        } else if(menuChoice==NEXT&&hasNext()){
            currentMenu = getNext();
        }
    }

    public Menu getCurrentMenu(){
        if(currentMenu.getDate() == null) setCurrent();
        return currentMenu;
    }

    public Menu getNext(){
        return menuEjb.getFutureMenu(currentMenu.getDate());
    }

    public boolean hasNext(){
        if(menuEjb.getFutureMenu(currentMenu.getDate())!=null){
            return true;
        }
        return false;
    }

    public Menu getPrevious(){
        return menuEjb.getPastMenu(currentMenu.getDate());
    }

    public boolean hasPrevious(){
        if(menuEjb.getPastMenu(currentMenu.getDate())!=null){
            return true;
        }
        return false;
    }

    public boolean hasMenus(){
        if(menuEjb.getMenuForDate(LocalDate.now())==null){
            return false;
        }
        else return true;
    }
}
