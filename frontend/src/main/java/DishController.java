import se.josoder.exam.backend.ejb.DishEjb;
import se.josoder.exam.backend.entity.Dish;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by josoder on 07.06.17.
 */
@Named
@SessionScoped
public class DishController implements Serializable{
    @EJB
    private DishEjb dishEjb;
    @Inject
    private LoginController loginController;

    private String formName;
    private String formDescription;

    public List<Dish> getAllDishes(){
        return dishEjb.getAllDishes();
    }

    public void addDish(){
        if(!loginController.isLoggedIn()){
            return;
        }
        try {
            dishEjb.createDish(formName, formDescription);
        } catch (Exception e){
        }
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormDescription() {
        return formDescription;
    }

    public void setFormDescription(String formDescription) {
        this.formDescription = formDescription;
    }
}
