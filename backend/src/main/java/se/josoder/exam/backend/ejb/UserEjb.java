package se.josoder.exam.backend.ejb;

import org.apache.commons.codec.digest.DigestUtils;
import se.josoder.exam.backend.entity.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by josoder on 08.06.17.
 */
@Stateless
public class UserEjb implements Serializable {
    @PersistenceContext
    protected EntityManager em;

    public UserEjb() {
    }


    public boolean createUser(String userId, String firstName, String lastName, String password){
        if(userId == null || userId.isEmpty() ||password == null || password.isEmpty()||
                firstName.isEmpty()||lastName.isEmpty()) {
            return false;
        }

        User u = findUser(userId);
        if(u!=null) return false;

        u = new User();
        u.setUserId(userId);
        u.setFirstName(firstName);
        u.setLastName(lastName);
        String salt = getSalt();
        u.setSalt(salt);
        String hash = computeHash(password, salt);
        u.setHash(hash);
        em.persist(u);
        return true;
    }


    public boolean userLogin(String username, String password){
        if(username==null||username.isEmpty()||password==null||password.isEmpty()){
            return false;
        }

        User u = findUser(username);
        if(u==null){
            return false;
        }
        if(!u.getHash().equals(computeHash(password, u.getSalt()))){
            return false;
        }else {
            return true;
        }
    }


    public User findUser(String userId){
        return em.find(User.class, userId);
    }



    @NotNull
    protected String computeHash(String password, String salt){
        String combined = password + salt;
        String hash = DigestUtils.sha256Hex(combined);
        return hash;
    }

    @NotNull
    protected String getSalt(){
        SecureRandom random = new SecureRandom();
        int bitsPerChar = 5;
        int twoPowerOfBits = 32; // 2^5
        int n = 26;
        assert n * bitsPerChar >= 128;

        String salt = new BigInteger(n * bitsPerChar, random).toString(twoPowerOfBits);
        return salt;
    }
}
