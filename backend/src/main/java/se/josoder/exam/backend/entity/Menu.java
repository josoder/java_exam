package se.josoder.exam.backend.entity;
import se.josoder.exam.backend.LocalDateConverter;

import javax.lang.model.element.Name;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by josoder on 07.06.17.
 */
@Entity
public class Menu {
    @Id @GeneratedValue
    private long id;

    @Column(unique = true)
    @Convert(converter = LocalDateConverter.class)
    private LocalDate date;

    @ManyToMany(fetch = FetchType.EAGER)
    @Size(min = 1)
    private Set<Dish> dishes;

    public Menu() {
        dishes = new HashSet<>();
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Set<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(Set<Dish> dishes) {
        this.dishes = dishes;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
