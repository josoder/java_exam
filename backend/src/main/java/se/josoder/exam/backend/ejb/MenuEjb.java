package se.josoder.exam.backend.ejb;
import se.josoder.exam.backend.LocalDateConverter;
import se.josoder.exam.backend.entity.Dish;
import se.josoder.exam.backend.entity.Menu;
import sun.security.util.ManifestEntryVerifier;

import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by josoder on 07.06.17.
 */
@Stateless
public class MenuEjb {
    @PersistenceContext
    private EntityManager em;


    public long createMenu(LocalDate date, Set<Dish> dishes){
        if(dishes.size()==0){
            throw new IllegalArgumentException("Cant save menu without dishes");
        }
        if(date==null){
            throw new IllegalArgumentException("date cant be null");
        }

        Menu menu = getMenuByDate(date);
        if(menu!=null){
            throw new IllegalArgumentException("the menu for the date" + date.toString() + " already exists");
        }

        menu = new Menu();
        menu.setDate(date);
        menu.setDishes(dishes);
        em.persist(menu);
        return menu.getId();
    }

    public Menu getMenuForDate(LocalDate date){
        Menu m = getMenuByDate(date);
        // if there is no menu for today
        if(m==null){
            m = getFutureMenu(LocalDate.now());
        }
        // if there is no menu today or in the future
        if(m==null) {
            m = getPastMenu(date);
        }
        return m;
    }

    public Menu getFutureMenu(LocalDate date){
        TypedQuery<Menu> q = em.createQuery("select m from Menu m where m.date > ?1" +
                " order by m.date asc", Menu.class);
        q.setParameter(1, date);
        Menu m = null;
        try {
            m = q.getResultList().get(0);
        } catch (Exception e){
            //e.printStackTrace(); (excessive logging)
        }
        return m;
    }

    public Menu getPastMenu(LocalDate date){
        TypedQuery<Menu> query = em.createQuery("select m from Menu m where m.date < ?1 " +
                "order by m.date desc", Menu.class);
        query.setParameter(1, date);
        Menu m = null;
        try {
            m = query.getResultList().get(0);
        } catch (Exception e){
            // e.printStackTrace(); (excessive logging)
        }
        return m;
    }


    //--------------------------------------------------------


    private Menu getMenuByDate(LocalDate date){
        TypedQuery<Menu> q = em.createQuery("select m from Menu m where m.date = ?1", Menu.class);
        q.setParameter(1, date);
        Menu m = null;
        try {
            m = q.getSingleResult();
        } catch (Exception e) {
            //e.printStackTrace(); (excessive logging)
        }
        return m;
    }

    private Dish getDish(String name){
        return em.find(Dish.class, name);
    }
}
