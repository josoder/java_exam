package se.josoder.exam.backend.ejb;

import se.josoder.exam.backend.entity.Dish;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.List;

/**
 * Created by josoder on 07.06.17.
 */
@Stateless
public class DishEjb {
    @PersistenceContext
    private EntityManager em;

    public void createDish(String name, String description){
        if(name==null||name.isEmpty()){
            throw new IllegalArgumentException("name cant be empty or null");
        }
        if(description==null||description.isEmpty()){
            throw new IllegalArgumentException("description cant be empty or null");
        }
        Dish dish = getDish(name);

        if(dish!=null){
            throw new IllegalArgumentException("dish already exists");
        }

        dish = new Dish();
        dish.setName(name);
        dish.setDescription(description);
        em.persist(dish);
    }


    public List<Dish> getAllDishes(){
        TypedQuery <Dish> allPosts = em.createQuery("select d from Dish d", Dish.class);
        return allPosts.getResultList();
    }

    public Dish getDish(String name){
        return em.find(Dish.class, name);
    }
}
