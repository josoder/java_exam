package se.josoder.exam.backend.entity;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by josoder on 07.06.17.
 */
@Entity
public class Dish {

    @Id @Size(min = 1, max = 100)
    private String name;

    @Size(min = 1, max = 300)
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
