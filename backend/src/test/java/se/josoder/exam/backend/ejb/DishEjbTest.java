package se.josoder.exam.backend.ejb;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJBException;

import static org.junit.Assert.assertEquals;

/**
 * Created by josoder on 07.06.17.
 */
@RunWith(Arquillian.class)
public class DishEjbTest extends EjbTestBase {
    @Test
    public void testCreateDish(){
        dishEjb.createDish("a new dish", "that contains something");
        assertEquals(1, dishEjb.getAllDishes().size());
    }

    @Test(expected = EJBException.class)
    public void createDishWithoutName(){
        dishEjb.createDish(null, "should fail");
    }

    @Test(expected = EJBException.class)
    public void createDishWithEmptyTitle(){
        dishEjb.createDish("", "should fail");
    }

    @Test(expected = EJBException.class)
    public void createDishWithEmptyDescription(){
        dishEjb.createDish("should fail", "");
    }

    @Test(expected = EJBException.class)
    public void createDishWihoutDescription(){
        dishEjb.createDish("should fail", null);
    }

    @Test
    public void testCreateTwoDishes(){
        // (see test base)
        createDish("salad");
        createDish("potato");
        assertEquals(2, dishEjb.getAllDishes().size());
    }
}
