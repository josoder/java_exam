package se.josoder.exam.backend.ejb;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;
import se.josoder.exam.backend.entity.Menu;

import javax.ejb.EJBException;
import java.time.LocalDate;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by josoder on 07.06.17.
 */
@RunWith(Arquillian.class)
public class MenuEjbTest extends EjbTestBase {
    @Test(expected = EJBException.class)
    public void testCreateMenuWithNoDish(){
        LocalDate date = LocalDate.of(2017,5,3);
        menuEjb.createMenu(date, new HashSet<>());
    }

    @Test
    public void testGetCurrentMenu(){
        // (see EjbTestBase)
        long menuId = createMenuWithTwoDishes(LocalDate.now());
        assertEquals(menuId, menuEjb.getMenuForDate(LocalDate.now()).getId());
    }

    @Test
    public void testGetAbsentPreviousMenu(){
        long pastMenuId = createMenuWithTwoDishes(LocalDate.of(2017, 5, 11));
        long pastMenuId2 = createMenuWithTwoDishes(LocalDate.of(2017,5, 16 ), "spaghetti", "tomato");
        long pastMenuId3 = createMenuWithTwoDishes(LocalDate.of(2017,5,17), "pizza", "kebab");
        assertEquals(pastMenuId3, menuEjb.getMenuForDate(LocalDate.now()).getId());
    }

    @Test(expected = EJBException.class)
    public void createMenuWithoutDate(){
        createMenuWithTwoDishes(null);
    }

    @Test
    public void testGetAbsentNextMenu(){
        long futureMenuId = createMenuWithTwoDishes(LocalDate.now().plusDays(1));
        assertEquals(futureMenuId, menuEjb.getMenuForDate(LocalDate.now()).getId());
    }

    @Test
    public void testGetAllDishesForMenu(){
        createMenuWithTwoDishes(LocalDate.now());
        Menu m = menuEjb.getMenuForDate(LocalDate.now());
        assertEquals(2, m.getDishes().size());
    }

    @Test
    public void testGetPreviousMenu(){
        long futureMenuId = createMenuWithTwoDishes(LocalDate.now().plusDays(2));
        long pastMenuId = createMenuWithTwoDishes(LocalDate.now().minusDays(2), "spaghetti", "bolognese");

        assertEquals(pastMenuId, menuEjb.getPastMenu(LocalDate.now()).getId());
    }

    @Test(expected = EJBException.class)
    public void testCreateMenuWithSameDate(){
        LocalDate date = LocalDate.now();
        assertNotNull(createMenuWithTwoDishes(date));
        createMenuWithTwoDishes(date);
    }

    @Test
    public void testThreeMenus(){
        LocalDate today = LocalDate.now();
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        LocalDate yesterday = LocalDate.now().minusDays(1);

        long todaysMenuId = createMenuWithTwoDishes(today, "sausages", "beans");
        long tommorrowsMenuId = createMenuWithTwoDishes(tomorrow,"pizza", "kebab");
        long yesterdaysMenuId = createMenuWithTwoDishes(yesterday, "taco", "sushi");

        assertEquals(tommorrowsMenuId, menuEjb.getFutureMenu(today).getId());
        assertEquals(yesterdaysMenuId, menuEjb.getPastMenu(today).getId());
        assertEquals(null, menuEjb.getFutureMenu(tomorrow));
        assertEquals(todaysMenuId, menuEjb.getPastMenu(tomorrow).getId());
        assertEquals(null, menuEjb.getPastMenu(yesterday));
        assertEquals(todaysMenuId, menuEjb.getFutureMenu(yesterday).getId());
    }

}
