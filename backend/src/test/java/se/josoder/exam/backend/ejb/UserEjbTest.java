package se.josoder.exam.backend.ejb;

import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.StringJoiner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by josoder on 05.06.17.
 */
@RunWith(Arquillian.class)
public class UserEjbTest extends EjbTestBase{
    @Test
    public void createUser(){
        String username = "TestUser";

        assertTrue(createUser(username));
    }

    @Test
    public void createUserWithoutUsername(){
        assertFalse(createUser(null));
        assertFalse(createUser(""));
    }

    @Test
    public void loginWithoutPassword(){
        assertTrue(createUser("testUser", "password"));
        assertFalse(userEjb.userLogin("testUser", null));
        assertFalse(userEjb.userLogin("testUser", ""));
    }

    @Test
    public void userLogin(){
        String username = "user";
        String password = "password";
        createUser(username, password);
        assertFalse(userEjb.userLogin(username, "wrongpassword"));
        assertTrue(userEjb.userLogin(username, password));
    }

    @Test
    public void passwordNotStoredInPlainText(){
        String password = "hej123";
        createUser("user",  password);
        assertFalse(userEjb.findUser("user").getHash().equals(password));
    }
}
