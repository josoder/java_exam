package se.josoder.exam.backend.ejb;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import se.josoder.exam.backend.entity.Dish;
import se.josoder.exam.backend.entity.Menu;
import se.josoder.exam.backend.util.DeleterEJB;
import se.josoder.exam.backend.entity.User;

import javax.ejb.EJB;
import java.time.LocalDate;
import java.util.HashSet;


public abstract class EjbTestBase {

    @Deployment
    public static JavaArchive createDeployment() {

        return ShrinkWrap.create(JavaArchive.class)
                .addPackages(true, "se.josoder.exam.backend")
                .addClass(DeleterEJB.class)
                .addPackages(true, "org.apache.commons.codec")
                .addAsResource("META-INF/persistence.xml");
    }

    @EJB
    protected UserEjb userEjb;

    @EJB
    protected DishEjb dishEjb;

    @EJB
    protected MenuEjb menuEjb;

    @EJB
    private DeleterEJB deleterEJB;


    @Before
    @After
    public void emptyDatabase(){
         deleterEJB.deleteEntities(Menu.class);
         deleterEJB.deleteEntities(Dish.class);
     //   postEJB.getAllPostsByTime().stream().forEach(p ->
     //           deleterEJB.deleteEntityById(Post.class, p.getId()));

        deleterEJB.deleteEntities(User.class);
    }

    protected long createMenuWithTwoDishes(LocalDate date) {
        createDish("salad");
        createDish("potato");
        HashSet<Dish> dishes = new HashSet<>();
        dishes.add(dishEjb.getDish("salad"));
        dishes.add(dishEjb.getDish("potato"));
        return menuEjb.createMenu(date, dishes);
    }

    protected long createMenuWithTwoDishes(LocalDate date, String... dish) {
        HashSet<Dish> dishes = new HashSet<>();
        for(String d:dish){
            createDish(d);
            dishes.add(dishEjb.getDish(d));
        }
        return menuEjb.createMenu(date, dishes);
    }

    protected void createDish(String name) { dishEjb.createDish(name, "food ??"); }

    protected boolean createUser(String user){
        return userEjb.createUser(user,"foo","b","c");
    }

    protected boolean createUser(String user, String password){
        return userEjb.createUser(user,"c","a",password);
    }
}
